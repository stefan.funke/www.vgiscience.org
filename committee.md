---
layout: page
title: Program Committee and Partner
menu: Committee
order: 7
---

# Consortium of the 1<sup>st</sup> funding period (2016-2019)

* [Prof. Dr. Dirk Burghardt](https://tu-dresden.de/bu/umwelt/geo/ifk/das-institut/beschaeftigte#section-0)  
    Technische Universität Dresden, Institute for Cartography

* [Prof. Dr. Daniel A. Keim](https://www.vis.uni-konstanz.de/en/members/keim/)  
    University of Konstanz, Data Analysis and Visualization

* [Prof. Dr. Bodo Rosenhahn](https://www.tnt.uni-hannover.de/staff/rosenhahn/)  
    Leibniz Universität Hannover, Institut für Informationsverarbeitung


# Consortium of the application submission

* [Prof. Dr. Dirk Burghardt](https://tu-dresden.de/bu/umwelt/geo/ifk/das-institut/beschaeftigte#section-0)  
    Technische Universität Dresden, Institute for Cartography

* [Prof. Dr. Wolfgang Nejdl](https://www.kbs.uni-hannover.de/~nejdl/)  
    Leibniz Universität Hannover, L3S Research Center

* [Prof. Dr. Jochen Schiewe](http://www.geomatik-hamburg.de/g2lab/schiewe.html)  
    HafenCity University Hamburg, Lab for Geoinformatics and Geovisualization (g2lab)

* [Prof. Dr. Monika Sester](https://www.ikg.uni-hannover.de/index.php?id=sester)  
    Leibniz Universität Hannover, Institute of Cartography and Geoinformatics

[Original specification (german)](spp/specification.html)
