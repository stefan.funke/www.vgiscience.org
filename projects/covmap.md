---
layout: page
---
COVMAP: Comprehensive Conjoint GPS and Video Data Analysis for Smart Maps
================================================================================

**Professor Carsten Rother**  
*Universität Heidelberg, Visual Learning Lab, Berliner Str. 43, 69120 Heidelberg*

**Dr.-Ing. Michael Ying Yang**  
*University of Twente Scene Understanding Group, Hengelosestraat 99, Enschede, The Netherlands*

**Professor Dr.-Ing. Bodo Rosenhahn**  
*Leibniz Universität Hannover, Fakultät für Elektrotechnik und Informatik, Institut für Informationsverarbeitung, Appelstraße 9a, 30167 Hannover*

During the last years the availability of spatial data has rapidly developed. Characteristic for this development is the involvement of a large number of users, who often use smart phones and mobile devices, to generate and make freely available Volunteered Geographic Information (VGI). E.g. Apps like Waze combine the local velocities of smart phones (in cars) to predict the flow velocities (and time delay) for traffic jams. Users can recommend and comment on specific traffic situations. Nonetheless, whereas GPS and gyroscope data (e.g. in fitness straps) are common, the demanding challenges for video analysis and the huge amount of data, which are easily collected with videos, make video analysis based methods very difficult. On the other hand, only videos allow for a comprehensive scene interpretation. In this research project, we are interested in combining GPS, gyroscope and video data to analyze road and traffic situations for cyclists and pedestrians. Our standard setting is a smart phone attached to a bicycle, which records the GPS coordinates, videos, (online) local weather information and time. Within eight work packages, we will (a) use the GPS-data for integration in a map, (b) The local velocities and gyroscope data as well as variations in the sensor data will be used to identify interesting situations during a bicycle ride, and (c) video data will be used to understand the specific situation which causes a delay in the ride. The analyzed data can be used for map enhancement and path recommendation, but also for the identification of unclear road marks which is important for city planning and accident avoidance. Besides collecting experience with real-time and event triggered data with a focus on a human-centered application, the foundations can easily be extended towards traffic management after hazards, quality control of topographic datasets or environmental and health-related data analysis using additional sensor information. With our application scenario in mind, three main research domains are especially relevant for the advancement of VGI, namely information retrieval and analysis of VGI, geovisualisation and cartographic communication, and social context. Our proposal is in the center of the aforementioned three research domains, in which scene parsing addresses information retrieval and analysis of VGI, map generation addresses geovisualisation and cartographic communication, and customer recommendation addresses social context. Thus, it fits perfectly into the priority programm.
