---
layout: page
---

# Visual analysis of volunteered geographic information for interactive situation modeling and real-time event assessment (VA4VGI 2)

**Professor Dr. Thomas Ertl & Dr. Steffen Koch**  
*Universität Stuttgart, Institut für Visualisierung und Interaktive Systeme, Abteilung Graphisch-Interaktive Systeme (GIS), Universitätsstraße 38, 70569 Stuttgart*

![](/images/va4vgi-fig1.jpg)  

In the [first phase of the project](va4vgi.md), we have been automatically collecting large volumes of geo-tagged Twitter data since 2011. At present, we are able to extract around 20 million tweets each day. We extended this corpus and complemented it with data from other sources than Twitter, including Foursquare, Facebook, and Reddit. We conducted various activities defined in our first phase roadmap which are currently in progress. One of our major first phase objectives was to establish and evaluate techniques for explorative regression model analytics. Based on the groundwork in scalable collection and processing of data from various sources, uncertainty aware visualizations, and integrated geo-inference. We designed a system architecture that we expect to evaluate with our domain expert collaborators in the remaining project duration. Furthermore, initial tests with our techniques for spatiotemporal situation representation have been promising and we expect to see final results in the
coming months.


Naturally, the volunteered geographic information (VGI) of the data - such as geotags or GPS locations provided by mobile phones - play a pivotal role in making it useful for situation assessment. As highlighted before, our collected data from these events as well as our thorough literature analysis showed the range of capabilities needed to improve the work in future iterations. From these observations as well as from the examination of related research we compiled three objectives that we propose to investigate in a second project phase:

* Collaborative model building: create visual interfaces for collaborative model building, deployment, monitoring, and adaptation.
* Spatial story tracking: visualize spatial event narratives by correlating social media with content form other sources.
* Malicious user network detection: provide enhanced schemes for the detection of malicious user activity based on spatial features.
