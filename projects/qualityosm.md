---
layout: page
---
A framework for measuring the fitness for purpose of OpenStreetMap data based on intrinsic quality indicators
================================================================================


**Dr.-Ing. Hongchao Fan**  
*Ruprecht-Karls-Universität Heidelberg, Geographisches Institut, Professur für Geoinformatik, Berliner Straße 48, 69120 Heidelberg*



The OpenStreetMap data in selected regions is normally evaluated by comparing with commercial or authority data, before it will be used for in a project in different application domains. However, there are three problems with this kind of so-called extrinsic quality assessment. Firstly, the results cannot directly answer the question about whether the OSM data is good enough for a certain purpose, because the extrinsic quality indicators are designed as standard to indicate an overall level of the quality. Secondly, the results may not be able to reflect the true quality of OSM data, because the existing tools (as those described by ISO) are not inclusive enough or appropriate to eloquently evaluate OSM data, because the nature of OSM, as a project of VGI, is fundamentally different to what geospatial experts have dealt with so far. Thirdly, the reference data is often not available due to contradictory licensing restrictions or high procurement costs.

In this project, we attempt to develop a framework that provides in a systematic way the methods and measures to evaluate the fitness for purpose of OSM data. A main objective is that this shall also be usable when there is no (authority) reference data for comparison is available, in order to make it applicable in a wide range of situations and extending the traditional approaches on spatial data quality evaluations based on comparison with other data sets. For this one sub-objective is to define systematically a taxonomy for different types of usage of OSM data in different application domains that includes the relevant indications and advises of: (i) what kind of information is required? (ii) what is the data quality requirements? (iii) which quality indicators can be used to evaluate the OSM data to be used? (iv) how can the quality indicators be calculated without using reference data? and (v) how creditable is the evaluation of the fitness for purpose? The framework is going to serve as procedure guidelines when using OSM data in projects in application domains such as urban planning, routing & navigation, disaster management, marketing, geocoding, map application, etc.
