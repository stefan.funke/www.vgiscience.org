---
layout: page
---
Human-Centered Relational Feature Classification for VGI
================================================================================


**Professor Christian Freksa, Ph.D.**  
*Universität Bremen, Fachbereich 3 Mathematik und Informatik, Arbeitsgruppe Cognitive Systems (CoSy), Postfach 330440, 28334 Bremen*



Volunteered Geographic Information (VGI) emphasizes the power of crowdsourcing as a means of data production and as a platform for exchanging geographic knowledge and providing services. In VGI, ordinary untrained citizens are involved in collecting, sharing, using, and maintaining spatial data – tasks for which previously trained experts in mapping agencies and national organizations were responsible. In this project, we will investigate data quality issues of collaborative mapping in VGI from an artificial intelligence / cognitive systems perspective. In the scope of our project, we focus on VGI as a potential source of land use and land cover mapping in which a potentially large number of citizens independently participate in mapping geographic features. For example, a piece of land covered by grass may be classified as "grassland" or more specifically as "park". From a cognitive perspective, the involvement of ordinary humans and the availability of a multitude of ways to describe geographic features pose a great challenge, as the diversity of backgrounds of the contributors may lead to incommensurable and/or faulty data.

In principle, there are two general ways to deal with this situation: (1) Normative or definition-based approach: Canonical feature categories are predefined; there is one and only one correct way to classify observations. Advantages: no ambiguities on the data level; data interpretation can be performed by application of the predefined norms. Disadvantage: Data may be erroneous, as it may be hard or impossible to correctly map the observations to the predefined categories due to incomplete information and / or incompatible granularity levels. (2) Human-centered approach: Volunteers use concepts they are most familiar with and are certain about to describe geographic features. Advantages: Granularity of description can be adapted to the level of observation; the information provided is more likely to be correct. Disadvantage: There is no obvious way to relate and compare observations of different contributors and to integrate the observations in a joint map. Currently, we have no techniques to combine the advantages of both approaches and to compensate for their drawbacks.
