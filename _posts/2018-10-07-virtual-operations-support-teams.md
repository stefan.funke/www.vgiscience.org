---
layout: post
title: "Virtual Operations Support Teams (VOST): An Innovative Approach to Disaster Management"
date: "2018-10-07 18:21:53 +0200"
author:
    - Ramian Fathi
    - Dennis Thom
    - Steffen Koch
    - Frank Fiedrich
---

The amount and variety of available Volunteered Geographic Information (VGI) in disaster management has been revolutionized with the emergence of social media services like Twitter, Facebook, Youtube, and Snapchat. Today, crisis responders can find huge amounts of timely eyewitness accounts, on-site photos and videos, crowd-behavioral information and situation awareness insights by analysing these platforms. Of course, these novel data channels can only be utilized if the experts find ways to cope with their large, noisy, and uncertain nature in order to turn that information overload into actionable insights.

To address that challenge, a new form of collaborative digital volunteering effort, called the Virtual Operations Support Team (VOST), has recently emerged in various countries around the globe. A VOST consists of members who are not only experts in social media analytics, web technologies, and information processing, but who also have significant experience and background in disaster management and crisis response. The team usually works dislocated from the actual site. However, it is often tightly connected or even integrated with the traditional disaster management institutions and sends technical advisors and liaison officers to the operational leadership or crisis unit. VOSTs often utilize various data mining and data visualization tools in order to monitor the huge real-time streams of data, verify potentially relevant entities, and represent insights in the form of interactive crisis maps and interactive reports.

For example, the German VOST (VOSTde) is associated with the Federal Agency for Technical Relief (THW) in Germany. In recent months, the VOSTde had various deployments, for example during the Grand Départ of Tour de France in Düsseldorf 2017 and the G20 summit in Hamburg 2017. 

Two active members of this Virtual Operations Support Team are DFG-VGI members: Dr. Dennis Thom from the University of Stuttgart and Ramian Fathi from the University of Wuppertal (see Figure1). In their recent VOST deployments, they supported the VOST with a prototype of a Visual Analytics software for social media analysis that was developed at the University of Stuttgart and further adapted and advanced in the context of the DFG-VGI project (see Figure 2).  

Apart from their voluntary services in VOSTde, Ramian and Dennis are both actively pursuing research in the fields of Visual Analytics, VGI data analytics, and digital volunteering. Dr. Thom and the team from University of Stuttgart focus on „Visual Analysis of Volunteered Geographic Information for Interactive Situation Modelling and Real-time Event Assessment“, whereas Ramian Fathi and the research group from Wuppertal University study the „Motivation and Participation of Digital Volunteer Communities in Humanitarian Assistance“.

![](/images/2018-10-07-virtual-operations-support-teams-1.jpg)
*Figure 1: Ramian Fathi and Dr. Dennis Thom during the deployment Grand Départ in Düsseldorf 2017. Source: THW*

![](/images/2018-10-07-virtual-operations-support-teams-2.jpg)
*Figure 2: Visual Social Media Analytics Software adapted for VOSTde*
