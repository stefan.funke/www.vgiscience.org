---
layout: page
---

# VGI Geovisual Analytics Workshop

Friday, October 19<sup>th</sup>, University of Konstanz, Germany  
[4th International Symposium on Big Data Visual and Immersive Analytics](https://bdva.net/2018/)

## Agenda

08:30 | Registration
09:00 | Welcome Notes + Keynote Speaker: [Prof. Jo Wood](http://bdva.net/2018/index.php/vgi-geovisual-analytics-workshop/#keynote)
10:00 | Modelling - Session chair: [Rahul Deb Das](https://www.geo.uzh.ch/geolean/en/units/gco/staff/?content=rahuldebdas)
      | **User Uncertainty: A Human-Centred Uncertainty Taxonomy for Volunteer Geographic Information through the Visual Analytics Workflow**<br>Alexandra Diehl, Bin Yang, Rahul Deb Das, Siming Chen, Gennady Andrienko, Natalia Andrienko, Doris Dransch and Daniel Keim
      | **A privacy-aware model to process data from location-based social media**<br>Marc Löchner, Alexander Dunkel and Dirk Burghardt      
      | **Classifying and visualizing the social facet of location-based social network data**<br>Thomas Gründemann and Dirk Burghardt
10:30 | Modelling: Q&A and Open discussions
10:45 | Morning Coffee Break
11:15 | Analytical Techniques - Session chair: [Bin Yang](https://www.gfz-potsdam.de/en/staff/bin-yang/sec15/)
      | **Analytical Comparison of Event-Driven Dynamic User Behaviours in Social Media with Visual Analytics**<br>Siming Chen, Natalia Andrienko and Gennady Andrienko
      | **Urban Traffic Event Detection using Twitter Data**<br>Rahul Deb Das and Ross Purves
      | **Understanding Collective Behavior with the Concept of Core Clusters**<br>Daniel Seebacher, Daniel Keim and Manuel Stein
      | **Analytical Workbench for Geo-inference Evaluations in Social Media Data** , Sanae Mahtal, Cristina Lupu, Benedikt Armbruster, Marvin Bechtold, Maximilian Reichel, Thomas Wangler, Dennis Thom, Steffen Koch and Thomas Ertl      
11:55 | Analytical Techniques: Q&A and Open discussions      
12:15 | Lunch
13:30 | Applications - Session chair: [Siming Chen](http://vis.pku.edu.cn/people/simingchen/)
      | **EveryAware Gears: A Tool to visualize and analyze all types of Citizen Science Data**<br>Florian Lautenschlager, Martin Becker, Michael Steininger and Andreas Hotho
      | **Evaluation of Spatial Data Layer Compositing Techniques in Interactive VR Environments**<br>Maxim Spur and Vincent Tourre
      | **Exploring geo-tagged photos to assess spatial patterns of visitors in protected areas: the case of park of Etna (Italy)**<br>Giovanni Signorello, Giovanni Maria Farinella, Lorenzo Di Silvestro, Alessandro Torrisi and Giovanni Gallo
14:00 | Applications: Q&A and Open discussions            
14:15 | Wrap-up and Conclusions
14:30 | Ending of the workshop

<style>
    td { vertical-align: baseline; }
    td:last-child { white-space:normal; }
    tr:nth-child(2n+1) { background-color: #6e717222; }
</style>
